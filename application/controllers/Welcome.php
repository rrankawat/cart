<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public function index() {
		$data['items'] = $this->welcome_model->all();

		$this->load->view('templates/header');
		$this->load->view('pages/index', $data);
		$this->load->view('templates/footer');
	}

	public function cart() {
		$data['cart_data'] = array();
		if(isset($_COOKIE['shopping_cart'])) {
			$cookie_data = stripcslashes($_COOKIE['shopping_cart']);
			$data['cart_data'] = json_decode($cookie_data, true);
			//echo "<pre>";print_r($item_data);die;
		}

		$this->load->view('templates/header');
		$this->load->view('pages/cart', $data);
		$this->load->view('templates/footer');
	}

	public function add() {
		$id = $this->input->post('id');
		$name = $this->input->post('name');
		$price = $this->input->post('price');
		$quantity = $this->input->post('quantity');

		if(!empty($this->input->post('add_to_cart'))) {

			if(isset($_COOKIE['shopping_cart'])) {
				$cookie_data = stripcslashes($_COOKIE['shopping_cart']);
				$cart_data = json_decode($cookie_data, true);
			}
			else {
				$cart_data = array();
			}

			$item_id_list = array_column($cart_data, 'id');
			if(in_array($id, $item_id_list)) {
				foreach ($cart_data as $key => $value) {
					if($cart_data[$key]['id'] == $id) {
						$cart_data[$key]['quantity'] = $cart_data[$key]['quantity'] + $quantity;
					}
				}
			}
			else {
				$item_array = array(
					'id' => $id,
					'name' => $name,
					'price' => $price,
					'quantity' => $quantity
				);
				$cart_data[] = $item_array;
			} 

			$item_data = json_encode($cart_data);
			setcookie('shopping_cart', $item_data, time() + (86400*30*7), '/cart/welcome');
			$this->session->set_flashdata('message', 'Added to cart');
			redirect('welcome/cart');
			//echo "<pre>";print_r($item_data);die;
		}
	}

	public function delete($id) {
		if(empty($id)) {
			show_404();
		}

		$cookie_data = stripcslashes($_COOKIE['shopping_cart']);
		$cart_data = json_decode($cookie_data, true);
		//echo "<pre>";print_r($cart_data);

		foreach ($cart_data as $key => $value) {
			if($cart_data[$key]['id'] == $id) {
				unset($cart_data[$key]);
				//echo "<pre>";print_r($cart_data);die;
				$item_data = json_encode($cart_data);
				setcookie('shopping_cart', $item_data, time() + (86400*30*7), '/cart/welcome');
				redirect('welcome/cart');
			}
		}
		
	}

}
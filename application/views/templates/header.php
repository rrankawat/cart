<!DOCTYPE html>
<html>
<head>
	<title>Webslesson Demo | Simple PHP Mysql Shopping Cart</title>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>
<body>
	<br />
	<div class="container">
	<br />

	<?php if($this->session->flashdata('message')): ?>
		<div class="alert alert-success" role="alert">
		  <?php echo $this->session->flashdata('message'); ?>
		</div>
	<?php endif; ?>
<h3 align="center">Simple PHP Mysql Shopping Cart using Cookies</h3><br />
<br />

<?php foreach($items as $item): ?>
	<div class="col-md-3">
		<div style="border:1px solid #333; background-color:#f1f1f1; border-radius:5px; padding:5px;" align="center">
			<img src="<?php echo base_url('assets/images/'.$item->image); ?>" class="img-responsive" /><br />
			<h4 class="text-info"><?php echo $item->name; ?></h4>
			<h4 class="text-danger">$ <?php echo $item->price; ?></h4>

			<?php echo form_open('welcome/add'); ?>
				<input type="hidden" name="id" value="<?php echo $item->id; ?>" />
				<input type="hidden" name="name" value="<?php echo $item->name; ?>" />
				<input type="hidden" name="price" value="<?php echo $item->price; ?>" />
				<input type="hidden" name="quantity" value="1" class="form-control" />
				<input type="submit" name="add_to_cart" style="margin-top:5px;" class="btn btn-success" value="Add to Cart" />
			<?php echo form_close(); ?>
		</div><br/>
	</div>
<?php endforeach; ?>
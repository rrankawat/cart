<h3>Order Details</h3>
<div class="table-responsive">
	<br/>

	<table class="table table-bordered">
		<tr>
			<th width="30%">Item Name</th>
			<th width="30%">Quantity</th>
			<th width="20%">Price</th>
			<th width="20%">Action</th>
		</tr>

		<?php if(isset($cart_data)): ?>
			<?php foreach($cart_data as $data): ?>
				<tr>
					<td><?php echo $data['name']; ?></td>
					<td><?php echo $data['quantity']; ?></td>
					<td>$ <?php echo $data['price']; ?></td>
					<td><a href="<?php echo base_url('welcome/delete/'.$data['id']); ?>">Delete</a></td>
				</tr>
			<?php endforeach; ?>
		<?php else: ?>
			<tr>
				<td colspan="4" class="text-center">No items to display</td>
			</tr>
		<?php endif; ?>

	</table>
</div>
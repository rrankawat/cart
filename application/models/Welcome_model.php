<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome_model extends CI_Model {

	public function all($id = NULL) {
		if(!empty($id)) {
			$this->db->select('*');
			$this->db->from('tbl_product');
			$this->db->where('id', $id);
			return $this->db->get()->result();
		}
		$this->db->select('*');
		$this->db->from('tbl_product');
		return $this->db->get()->result();
	}
}
